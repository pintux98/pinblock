package it.pintux98.PinBlock.util;

import it.pintux98.PinBlock.main;

import java.util.HashMap;

public class MessageUtil {

    private HashMap<String, String> messages;

    public MessageUtil() {
        messages = new HashMap<>();
    }

    public void loadMessage(String key, String value) {
        messages.putIfAbsent(key, value);
    }

    public HashMap<String, String> getMessages() {
        return messages;
    }

    public String message(String input) {
        String temp = main.getInst().color(getMessages().get(input));
        return temp != null ? temp : "INVALID";
    }
}

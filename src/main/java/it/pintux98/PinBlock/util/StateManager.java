package it.pintux98.PinBlock.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

public class StateManager {

    private static ConcurrentHashMap<Integer, Object> packets = new ConcurrentHashMap<>();

    public static String getServerVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().substring(23);
    }

    public static Class<?> getClass(String classname) {
        String version = getServerVersion();
        try {
            String path = classname.replace("{nms}", "net.minecraft.server." + version)
                    .replace("{nm}", "net.minecraft." + version).replace("{cb}", "org.bukkit.craftbukkit." + version)
                    .replace("{b}", "org.bukkit");
            return Class.forName(path);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Object getNmsPlayer(Player p) {
        if (p == null) {
            return null;
        }
        Method getHandle;
        try {
            getHandle = p.getClass().getMethod("getHandle");
            return getHandle.invoke(p);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void sendPlayerPacket(Player p, Object packet) throws Exception {
        Object nmsPlayer = getNmsPlayer(p);
        Object connection = nmsPlayer.getClass().getField("playerConnection").get(nmsPlayer);
        connection.getClass().getMethod("sendPacket", getClass("{nms}.Packet")).invoke(connection, packet);
    }

    public static boolean setGamemode(Player player, int gamemode) {
        Object gamemodepacket = packets.compute(gamemode, (integer, o) -> {
            if (o != null) {
                return o;
            }
            try {
                o = getClass("{nms}.PacketPlayOutGameStateChange").getConstructor(int.class, float.class).newInstance(3, gamemode);
            } catch (Exception e) {
                e.printStackTrace();
                return true;
            }
            return o;
        });

        try {
            sendPlayerPacket(player, gamemodepacket);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}

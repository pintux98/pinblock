package it.pintux98.PinBlock.util;

import it.pintux98.PinBlock.main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class UtilManager {
    private final String key = "p1nbl0ck";
    private final String staffpex = "pinblock.staff";
    private final String gmcpex = "pinblock.gamemode";
    private final String menuKey = "p1ntuxm3nu";
    private final main plugin;

    public UtilManager(main plugin) {
        this.plugin = plugin;
    }

    public String getGmcpex() {
        return gmcpex;
    }

    public boolean match(ItemStack item, Player p) {
        if (item != null) {
            String value = NBTEditor.getString(item, key);
            if (value != null && !value.isEmpty()) {
                return value.equalsIgnoreCase(p.getName()) || p.hasPermission(staffpex) || p.isOp();
            }
        }
        return false;
    }

    public ItemStack setTag(ItemStack item, Player p) {
        ItemStack tagItem;
        if (!hasTag(item)) {
            tagItem = NBTEditor.set(item, p.getName(), key);
            addLore(tagItem, p);
        } else {
            tagItem = item;
        }
        return tagItem;
    }

    public ItemStack setMenuTag(ItemStack item, String data) {
        ItemStack tagItem;
        if (!hasTag(item)) {
            tagItem = NBTEditor.set(item, data, menuKey);
        } else {
            tagItem = item;
        }
        return tagItem;
    }

    public String getMenuTag(ItemStack item) {
        if (item != null) {
            String value = NBTEditor.getString(item, menuKey);
            if (value != null && !value.isEmpty()) {
                return value;
            }
        }
        return null;
    }

    public Player getTag(ItemStack item) {
        if (item != null) {
            String value = NBTEditor.getString(item, key);
            if (value != null && !value.isEmpty()) {
                return Bukkit.getPlayer(value);
            }
        }
        return null;
    }

    public boolean hasTag(ItemStack item) {
        if (item != null) {
            String value = NBTEditor.getString(item, key);
            return value != null && !value.isEmpty();
        }
        return false;
    }

    public void addLore(ItemStack item, Player player) {
        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>();
        lore.add(plugin.getMessageUtil().message("CREATIVE_ITEM").replaceAll("%player%", player.getName()));
        meta.setLore(lore);
        item.setItemMeta(meta);
    }

    public String getStaffpex() {
        return staffpex;
    }

    public Location getLocation(Player player) {

        Location location = player.getLocation();
        int i = 0;
        final World world = location.getWorld();
        final Block highest = world != null ? world.getHighestBlockAt(location).getRelative(BlockFace.DOWN) : null;
        Block block = highest != null && highest.getY() < location.getY() ? highest : location.getBlock();
        while (!block.getType().isSolid() && block.getLocation().getY() >= 0) {
            block = block.getRelative(BlockFace.DOWN);
            i++;
        }
        return new Location(location.getWorld(), location.getX(), block.getY() >= 0 ? block.getY() + 1 : location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }
}

package it.pintux98.PinBlock.util;

public enum PBVersion {

    OLD("old"), //1.8
    COMBAT("combat"), //1.9
    NEW("new"), //1.10 and others till 1.16
    UNKNOWN("unknown"); //Unknown version

    private String version;

    PBVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}

package it.pintux98.PinBlock.util;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class PBPlayer {

    private final Player player;
    private boolean inGamemode;


    public PBPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isInGamemode() {
        return inGamemode;
    }

    public void setInGamemode(boolean inGamemode) {
        this.inGamemode = inGamemode;
    }

    public void set() {
        StateManager.setGamemode(player, -1);
        player.setAllowFlight(true);
        player.setFlying(true);
        player.setInvulnerable(true);
        setInGamemode(true);
    }

    public void unset() {
        player.setGameMode(GameMode.SURVIVAL);
        player.setAllowFlight(false);
        player.setFlying(false);
        player.setInvulnerable(false);
        setInGamemode(false);
    }
}

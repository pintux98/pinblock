package it.pintux98.PinBlock.commands;

import it.pintux98.PinBlock.main;
import it.pintux98.PinBlock.util.PBPlayer;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Iterator;

public class GamemodeCommand implements CommandExecutor {

    private final main plugin;

    public GamemodeCommand(main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {

            Player p = (Player) sender;

            if (p.hasPermission("pinblock.gamemode")) {
                if (plugin.getPbPlayers().stream().anyMatch(pbPlayer -> pbPlayer.getPlayer().equals(p))) {
                    p.sendMessage(plugin.color(plugin.getMessageUtil().message("GAMEMODE_SWITCH").replaceAll("%mode%", "SURVIVAL")));
                    Iterator<PBPlayer> iterator = plugin.getPbPlayers().iterator();
                    while (iterator.hasNext()) {
                        PBPlayer pbPlayer = iterator.next();
                        if (pbPlayer.getPlayer().equals(p)) {
                            pbPlayer.unset();
                            break;
                        }
                    }
                    p.getInventory().setItem(main.getInst().getConfig().getInt("menu.slot"), new ItemStack(Material.AIR));
                    p.teleport(plugin.getUtil().getLocation(p));
                } else {
                    p.sendMessage(plugin.color(plugin.getMessageUtil().message("GAMEMODE_SWITCH").replaceAll("%mode%", "CREATIVE")));
                    PBPlayer pbPlayer = new PBPlayer(p);
                    pbPlayer.set();
                    plugin.getPbPlayers().add(pbPlayer);
                    ItemStack item = new ItemStack(Material.CARROT);
                    p.getInventory().setItem(8, item);
                }
            } else {
                p.sendMessage(plugin.color(main.getInst().getConfig().getString("Messages.NO_PEX")));
            }
        }
        return false;
    }
}

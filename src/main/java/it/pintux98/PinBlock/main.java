package it.pintux98.PinBlock;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import it.pintux98.PinBlock.commands.GamemodeCommand;
import it.pintux98.PinBlock.manager.BlockManager;
import it.pintux98.PinBlock.manager.InventoryManager;
import it.pintux98.PinBlock.manager.PlayerManager;
import it.pintux98.PinBlock.menu.PBMenuListener;
import it.pintux98.PinBlock.menu.PlayerMenuUtility;
import it.pintux98.PinBlock.menu.models.MenuModelManager;
import it.pintux98.PinBlock.util.MessageUtil;
import it.pintux98.PinBlock.util.PBPlayer;
import it.pintux98.PinBlock.util.PBVersion;
import it.pintux98.PinBlock.util.UtilManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class main extends JavaPlugin {
    private static main inst;
    private final ConcurrentHashMap<Player, PlayerMenuUtility> playerMenuUtilityMap = new ConcurrentHashMap<>();
    private final List<PBPlayer> pbPlayers = new ArrayList<>();
    private UtilManager util;
    private Economy econ = null;
    private MessageUtil messageUtil;
    private MenuModelManager modelManager;
    private WorldGuardPlugin worldGuardPlugin;

    public static main getInst() {
        return inst;
    }

    public WorldGuardPlugin getWorldGuardPlugin() {
        return worldGuardPlugin;
    }

    public List<PBPlayer> getPbPlayers() {
        return pbPlayers;
    }

    public UtilManager getUtil() {
        return util;
    }

    public String color(String input) {
        return ChatColor.translateAlternateColorCodes('&', input);
    }

    public PlayerMenuUtility getPlayerMenuUtility(Player player) {
        PlayerMenuUtility playerMenuUtility;
        if (playerMenuUtilityMap.containsKey(player)) {
            return playerMenuUtilityMap.get(player);
        } else {
            playerMenuUtility = new PlayerMenuUtility(player);
            playerMenuUtilityMap.put(player, playerMenuUtility);
            return playerMenuUtility;
        }
    }

    public PBVersion getVersion() {
        PBVersion version;
        if (Bukkit.getVersion().contains("1.8")) {
            version = PBVersion.OLD;
        } else if (Bukkit.getVersion().contains("1.13") || Bukkit.getVersion().contains("1.14")
                || Bukkit.getVersion().contains("1.15") || Bukkit.getVersion().contains("1.16")) {
            version = PBVersion.NEW;
        } else if (Bukkit.getVersion().contains("1.9")) {
            version = PBVersion.COMBAT;
        } else {
            version = PBVersion.UNKNOWN;
        }
        return version;
    }

    public MessageUtil getMessageUtil() {
        return messageUtil;
    }

    @Override
    public void onEnable() {
        if (getServer().getPluginManager().getPlugin("Vault") != null) {
            worldGuardPlugin = getWorldGuard();
            main.inst = this;
            setupEconomy();
            getCommand("gmc").setExecutor(new GamemodeCommand(this));
            this.loadConfig();
            this.regEvents();
            loadMessages();
            getVersion();
            util = new UtilManager(this);
            modelManager = new MenuModelManager(this);
            modelManager.loadCreativeModel();
            modelManager.loadShopModel();
            modelManager.loadMenuModels();
            modelManager.loadMenuOptionModels();
            getLogger().info("Loaded " + modelManager.getCreativeModel().getOptions().size() + " creative options");
            getLogger().info("Loaded " + modelManager.getShopModel().getShops().size() + " shops");
            getLogger().info("Loaded " + modelManager.getMenuItemModels().size() + " menu items");
            getLogger().info("Loaded " + modelManager.getMenuOption().getOptions().size() + " menu options");
        } else {
            getServer().getLogger().info("Vault not found ... disabling plugin");
            getServer().getPluginManager().disablePlugin(this);
        }
    }

    public void loadMessages() {
        messageUtil = new MessageUtil();
        if (!messageUtil.getMessages().isEmpty()) {
            messageUtil.getMessages().clear();
        }
        for (String key : getConfig().getConfigurationSection("Messages").getKeys(false)) {
            String value = getConfig().getString("Messages." + key);
            messageUtil.loadMessage(key, value);
        }
    }

    @Override
    public void onDisable() {
        for (PBPlayer pbPlayer : pbPlayers) {
            Player p = pbPlayer.getPlayer();
            pbPlayer.unset();
            p.getInventory().setItem(getConfig().getInt("menu.slot"), new ItemStack(Material.AIR));
            p.teleport(getUtil().getLocation(p));
        }
        pbPlayers.clear();
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    private void regEvents() {
        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(new InventoryManager(this), this);
        pm.registerEvents(new PlayerManager(this), this);
        pm.registerEvents(new BlockManager(this), this);
        pm.registerEvents(new PBMenuListener(), this);
    }

    private void loadConfig() {
        File config = new File(getDataFolder(), "config.yml");
        if (!config.exists()) {
            config.getParentFile().mkdirs();
            saveDefaultConfig();
        }
    }

    private WorldGuardPlugin getWorldGuard() {
        Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            return null;
        }
        return (WorldGuardPlugin) plugin;
    }

    public MenuModelManager getModelManager() {
        return modelManager;
    }

    public Economy getEcon() {
        return econ;
    }
}

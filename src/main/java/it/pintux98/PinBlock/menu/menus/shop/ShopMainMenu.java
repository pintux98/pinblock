package it.pintux98.PinBlock.menu.menus.shop;

import it.pintux98.PinBlock.menu.PBMenu;
import it.pintux98.PinBlock.menu.PlayerMenuUtility;
import it.pintux98.PinBlock.menu.models.shop.ShopModel;
import it.pintux98.PinBlock.menu.models.shop.ShopOption;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

public class ShopMainMenu extends PBMenu {

    private final ShopModel model;

    public ShopMainMenu(PlayerMenuUtility playerMenuUtility) {
        super(playerMenuUtility);
        model = plugin.getModelManager().getShopModel();
    }

    @Override
    public String getMenuName() {
        return model.getTitle();
    }

    @Override
    public int getRows() {
        return 6;
    }

    @Override
    public void handleMenu(InventoryClickEvent e) {
        if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
            for (ShopOption shop : model.getShops()) {
                String tag = plugin.getUtil().getMenuTag(e.getCurrentItem());
                if (tag != null) {
                    if (plugin.getUtil().getMenuTag(e.getCurrentItem()).equalsIgnoreCase(shop.getShop())) {
                        new ShopOptionMenu(playerMenuUtility, shop, model).open();
                    }
                }
            }
        }
    }

    @Override
    public void setMenuItems() {
        for (ShopOption shop : model.getShops()) {
            inventory.setItem(shop.getSlot(), plugin.getUtil().setMenuTag(shop.getIcon(), shop.getShop()));
        }
        for (int i = 0; i < inventory.getSize(); i++) {
            if (inventory.getItem(i) == null || inventory.getItem(i).getType() == Material.AIR) {
                inventory.setItem(i, super.FILLER_GLASS);
            }
        }
    }
}

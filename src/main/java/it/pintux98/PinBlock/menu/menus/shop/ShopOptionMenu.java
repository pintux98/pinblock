package it.pintux98.PinBlock.menu.menus.shop;

import it.pintux98.PinBlock.menu.PBPaginatedMenu;
import it.pintux98.PinBlock.menu.PlayerMenuUtility;
import it.pintux98.PinBlock.menu.models.shop.*;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShopOptionMenu extends PBPaginatedMenu {

    private final ShopOption shop;
    private final ShopModel model;
    private HashMap<String, ItemStack> menuItems;

    public ShopOptionMenu(PlayerMenuUtility playerMenuUtility, ShopOption shop, ShopModel model) {
        super(playerMenuUtility);
        this.shop = shop;
        this.model = model;
        menuItems = plugin.getModelManager().getMenuOption().getOptions();
    }

    @Override
    public String getMenuName() {
        return shop.getTitle();
    }

    @Override
    public int getRows() {
        return 6;
    }

    @Override
    public void handleMenu(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();
        if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) {
            return;
        }
        if (e.getCurrentItem().equals(super.FILLER_GLASS)) {
            return;
        }
        List<ShopItem> items = shop.getItems();
        if (e.getCurrentItem().equals(menuItems.get("close"))) {
            new ShopMainMenu(playerMenuUtility).open();
        } else if (e.getCurrentItem().equals(menuItems.get("left"))) {
            if (page == 0) {
                p.sendMessage(plugin.getMessageUtil().message("FIRST_SHOP_PAGE"));
            } else {
                page = page - 1;
                super.open();
            }
        } else if (e.getCurrentItem().equals(menuItems.get("right"))) {
            if (!((index + 1) >= items.size())) {
                page = page + 1;
                super.open();
            } else {
                p.sendMessage(plugin.getMessageUtil().message("LAST_SHOP_PAGE"));
            }
        } else {
            ShopMenuOption menuOption = e.getClick() == ClickType.LEFT ? findMenuOption("buy") : e.getClick() == ClickType.RIGHT ? findMenuOption("sell") : null;
            if (menuOption != null) {
                if (p.hasPermission(menuOption.getPermission())) {
                    for (ShopItem shitem : items) {
                        if (e.getCurrentItem().equals(shitem.getItem())) {
                            new ShopSelectionMenu(playerMenuUtility, menuOption, shop, shitem).open();
                        }
                    }
                } else {
                    p.sendMessage(plugin.getMessageUtil().message("NO_SHOP_PEX"));
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
        }

    }

    @Override
    public void setMenuItems() {
        addMenuBorder();
        List<ShopItem> items = shop.getItems();
        for (int i = 0; i <= super.maxItemsPerPage; i++) {
            index = super.maxItemsPerPage * page + i;
            if (index >= items.size()) break;
            ShopItem shopItem = items.get(index);
            ItemStack item = shopItem.getItem();
            ItemMeta meta = item.getItemMeta();
            List<String> lore = new ArrayList<>();
            lore.add("");
            lore.add(plugin.color("&6&lBuyprice: &a&l" + shopItem.getBuyprice() + (shopItem.getShopCurency() == ShopCurrency.BALANCE ? "$" : shopItem.getShopCurency() == ShopCurrency.EXP ? "EXP" : "TOKENS")));
            lore.add(plugin.color("&6&lSellprice: &a&l" + shopItem.getSellprice() + (shopItem.getShopCurency() == ShopCurrency.BALANCE ? "$" : shopItem.getShopCurency() == ShopCurrency.EXP ? "EXP" : "TOKENS")));
            lore.add("");
            meta.setLore(lore);
            item.setItemMeta(meta);
            inventory.addItem(item);
        }
    }

    private ShopMenuOption findMenuOption(String option) {
        for (ShopMenuOption menuOption : model.getShopMenuOptions()) {
            if (menuOption.getOption().equalsIgnoreCase(option)) {
                return menuOption;
            }
        }
        return null;
    }
}

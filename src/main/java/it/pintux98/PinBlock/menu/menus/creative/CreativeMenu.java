package it.pintux98.PinBlock.menu.menus.creative;

import it.pintux98.PinBlock.menu.PBPaginatedMenu;
import it.pintux98.PinBlock.menu.PlayerMenuUtility;
import it.pintux98.PinBlock.menu.models.creative.CreativeOption;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class CreativeMenu extends PBPaginatedMenu {

    private final List<ItemStack> items;
    private final CreativeOption option;

    public CreativeMenu(PlayerMenuUtility playerMenuUtility, CreativeOption option) {
        super(playerMenuUtility);
        this.option = option;
        items = option.getContent();
    }

    @Override
    public String getMenuName() {
        return option.getTitle();
    }

    @Override
    public int getRows() {
        return 6;
    }

    @Override
    public void handleMenu(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) {
            return;
        }
        if (e.getCurrentItem().equals(super.FILLER_GLASS)) {
            return;
        }
        if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            new CategoryMenu(playerMenuUtility).open();
        } else if (e.getCurrentItem().getType().equals(Material.WOOD_BUTTON)) {
            if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Left")) {
                if (page == 0) {
                    p.sendMessage(plugin.getMessageUtil().message("FIRST_SHOP_PAGE"));
                } else {
                    page = page - 1;
                    super.open();
                }
            } else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Right")) {
                if (!((index + 1) >= items.size())) {
                    page = page + 1;
                    super.open();
                } else {
                    p.sendMessage(plugin.getMessageUtil().message("LAST_SHOP_PAGE"));
                }
            }
        } else {
            if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
                ItemStack itemStack = e.getCurrentItem();
                itemStack.setAmount(e.getCurrentItem().getMaxStackSize());
                p.getInventory().addItem(plugin.getUtil().setTag(itemStack, p));
            }
        }
    }

    @Override
    public void setMenuItems() {
        addMenuBorder();
        if (!items.isEmpty()) {
            for (int i = 0; i <= super.maxItemsPerPage; i++) {
                index = super.maxItemsPerPage * page + i;
                if (index >= items.size()) break;
                ItemStack itemStack = new ItemStack(items.get(index));
                ItemMeta itemMeta = itemStack.getItemMeta();
                itemMeta.setDisplayName(ChatColor.GOLD + itemStack.getType().toString());
                itemStack.setItemMeta(itemMeta);
                itemStack.setAmount(itemStack.getMaxStackSize());
                inventory.addItem(itemStack);
            }
        }
    }
}

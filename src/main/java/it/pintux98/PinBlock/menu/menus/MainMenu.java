package it.pintux98.PinBlock.menu.menus;

import it.pintux98.PinBlock.main;
import it.pintux98.PinBlock.menu.PBMenu;
import it.pintux98.PinBlock.menu.PlayerMenuUtility;
import it.pintux98.PinBlock.menu.models.menu.MenuItemModel;
import it.pintux98.PinBlock.menu.models.menu.MenuLocation;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class MainMenu extends PBMenu {

    public MainMenu(PlayerMenuUtility playerMenuUtility) {
        super(playerMenuUtility);
    }

    @Override
    public String getMenuName() {
        return plugin.getModelManager().getMenuOption().getTitle();
    }

    @Override
    public int getRows() {
        return 3;
    }

    @Override
    public void handleMenu(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        for (MenuItemModel model : plugin.getModelManager().getMenuItemModels()) {
            if (e.getCurrentItem().equals(model.getItem())) {
                try {
                    Class<? extends PBMenu> cl = main.getInst().getModelManager().getMenus().get(model.getOpenMenu());
                    Constructor constructor = cl.getConstructor(PlayerMenuUtility.class);
                    Object finale = constructor.newInstance(main.getInst().getPlayerMenuUtility(p));
                    PBMenu menu = cl.cast(finale);
                    menu.open();
                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException ex) {
                    ex.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    public void setMenuItems() {
        for (MenuItemModel model : plugin.getModelManager().getMenuItemModels()) {
            if (model.getLocation() == MenuLocation.MAIN_MENU) {
                inventory.setItem(model.getSlot(), model.getItem());
            }
        }
        for (int i = 0; i < inventory.getSize(); i++) {
            if (inventory.getItem(i) == null || inventory.getItem(i).getType() == Material.AIR) {
                inventory.setItem(i, super.FILLER_GLASS);
            }
        }
    }
}

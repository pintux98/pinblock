package it.pintux98.PinBlock.menu.menus.shop;

import it.pintux98.PinBlock.menu.PBMenu;
import it.pintux98.PinBlock.menu.PlayerMenuUtility;
import it.pintux98.PinBlock.menu.models.shop.ShopCurrency;
import it.pintux98.PinBlock.menu.models.shop.ShopItem;
import it.pintux98.PinBlock.menu.models.shop.ShopMenuOption;
import it.pintux98.PinBlock.menu.models.shop.ShopOption;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ShopSelectionMenu extends PBMenu {

    private final ShopItem shopItem;
    private final ShopMenuOption menuOption;
    private final ShopOption shop;

    public ShopSelectionMenu(PlayerMenuUtility playerMenuUtility, ShopMenuOption menuOption, ShopOption shop, ShopItem item) {
        super(playerMenuUtility);
        this.menuOption = menuOption;
        this.shopItem = item;
        this.shop = shop;
    }

    @Override
    public String getMenuName() {
        return menuOption.getTitle();
    }

    @Override
    public int getRows() {
        return 5;
    }

    @Override
    public void handleMenu(InventoryClickEvent e) {
        if (e.getCurrentItem().equals(super.FILLER_GLASS)) {
            return;
        }
        if (e.getCurrentItem().equals(plugin.getModelManager().getMenuOption().getOptions().get("close"))) {
            new ShopOptionMenu(playerMenuUtility, shop, plugin.getModelManager().getShopModel()).open();
            return;
        }
        String tag = plugin.getUtil().getMenuTag(e.getCurrentItem());
        String value = tag.split(":")[0];
        int amount = Integer.parseInt(tag.split(":")[1]);
        if (value.equalsIgnoreCase("BUY")) {
            if (hasSpaceForItem((Player) e.getWhoClicked(), e.getCurrentItem().getType(), amount)) {
                for (ShopItem shopItem : shop.getItems()) {
                    if (shopItem.getItem().getType() == e.getCurrentItem().getType()) {
                        switch (shopItem.getShopCurency()) {
                            case EXP:
                                if (((Player) e.getWhoClicked()).getExp() >= (shopItem.getBuyprice() * amount)) {
                                    ((Player) e.getWhoClicked()).setLevel(((Player) e.getWhoClicked()).getLevel() - (shopItem.getBuyprice() * amount));
                                    e.getWhoClicked().getInventory().addItem(new ItemStack(e.getCurrentItem().getType(), amount));
                                    int space = getEmptySlots(playerMenuUtility.getOwner()) * shopItem.getItem().getMaxStackSize();
                                    inventory.setItem(24, plugin.getUtil().setMenuTag(createItem(shopItem, space), "BUY:" + space));
                                    for (String command : shopItem.getBuycommands()) {
                                        try {
                                            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command.replaceAll("%player%", ((Player) e.getWhoClicked()).getDisplayName()).replaceAll("%ammount%", String.valueOf(amount)).replaceAll("%item%", e.getCurrentItem().getType().toString()));
                                        } catch (Exception ignored) {
                                        }
                                    }
                                } else {
                                    e.getWhoClicked().sendMessage(plugin.getMessageUtil().message("NO_BALANCE").replaceAll("%currency%", shopItem.getShopCurency().name()));
                                }
                                break;
                            case TOKENS:
                                break;
                            case BALANCE:
                                EconomyResponse response = plugin.getEcon().withdrawPlayer((Player) e.getWhoClicked(), shopItem.getBuyprice() * amount);
                                if (response.type == EconomyResponse.ResponseType.SUCCESS) {
                                    e.getWhoClicked().getInventory().addItem(new ItemStack(e.getCurrentItem().getType(), amount));
                                    int space = getEmptySlots(playerMenuUtility.getOwner()) * shopItem.getItem().getMaxStackSize();
                                    inventory.setItem(24, plugin.getUtil().setMenuTag(createItem(shopItem, space), "BUY:" + space));
                                    for (String command : shopItem.getBuycommands()) {
                                        try {
                                            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command.replaceAll("%player%", ((Player) e.getWhoClicked()).getDisplayName()).replaceAll("%ammount%", String.valueOf(amount)).replaceAll("%item%", e.getCurrentItem().getType().toString()));
                                        } catch (Exception ignored) {
                                        }
                                    }
                                } else {
                                    e.getWhoClicked().sendMessage(plugin.getMessageUtil().message("NO_BALANCE").replaceAll("%currency%", shopItem.getShopCurency().name()));
                                }
                                break;
                            case INVALID:
                            default:
                                return;
                        }
                    }
                }
            } else {
                e.getWhoClicked().sendMessage(plugin.getMessageUtil().message("NO_SPACE"));
            }
        } else if (value.equalsIgnoreCase("SELL")) {
            for (ShopItem shopItem : shop.getItems()) {
                if (shopItem.getItem().getType() == e.getCurrentItem().getType()) {
                    if (removeItems(e.getWhoClicked().getInventory(), e.getCurrentItem().getType(), amount)) {
                        switch (shopItem.getShopCurency()) {
                            case EXP:
                                ((Player) e.getWhoClicked()).setLevel(((Player) e.getWhoClicked()).getLevel() + (shopItem.getBuyprice() * amount));
                                break;
                            case TOKENS:
                                break;
                            case BALANCE:
                                EconomyResponse response = plugin.getEcon().depositPlayer((Player) e.getWhoClicked(), shopItem.getBuyprice() * amount);
                                if (response.type == EconomyResponse.ResponseType.SUCCESS) {
                                    for (String command : shopItem.getSellcommands()) {
                                        try {
                                            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command.replaceAll("%player%", ((Player) e.getWhoClicked()).getDisplayName()).replaceAll("%ammount%", String.valueOf(amount)).replaceAll("%item%", e.getCurrentItem().getType().toString()));
                                        } catch (Exception ignored) {
                                        }
                                    }
                                } else {
                                    plugin.getLogger().info("An error occurred while trying to give " + e.getWhoClicked().getName() + shopItem.getBuyprice() * amount + " balance!");
                                    return;
                                }
                                break;
                            case INVALID:
                            default:
                                return;
                        }
                        int space = getAmount(playerMenuUtility.getOwner().getInventory(), shopItem.getItem().getType());
                        if (space > 0) {
                            if (space < e.getCurrentItem().getMaxStackSize()) {
                                inventory.setItem(22, super.FILLER_GLASS);
                            }
                            inventory.setItem(24, plugin.getUtil().setMenuTag(createItem(shopItem, space), "SELL:" + space));
                        } else {
                            e.getWhoClicked().sendMessage(plugin.getMessageUtil().message("NO_ITEMS"));
                        }
                    } else {
                        e.getWhoClicked().sendMessage(plugin.getMessageUtil().message("NO_ITEMS"));
                    }
                }
            }
        }
    }

    @Override
    public void setMenuItems() {
        inventory.setItem(20, plugin.getUtil().setMenuTag(createItem(shopItem, 1), menuOption.getOption().toUpperCase() + ":" + 1));
        if (menuOption.getOption().toUpperCase().equalsIgnoreCase("BUY")) {
            int amount = getEmptySlots(playerMenuUtility.getOwner()) * shopItem.getItem().getMaxStackSize();
            inventory.setItem(22, plugin.getUtil().setMenuTag(createItem(shopItem, shopItem.getItem().getMaxStackSize()), menuOption.getOption().toUpperCase() + ":" + shopItem.getItem().getMaxStackSize()));
            inventory.setItem(24, plugin.getUtil().setMenuTag(createItem(shopItem, amount), "BUY:" + amount));
        } else {
            int amount = getAmount(playerMenuUtility.getOwner().getInventory(), shopItem.getItem().getType());
            if (amount > 0) {
                if (amount < shopItem.getItem().getMaxStackSize()) {
                    inventory.setItem(22, super.FILLER_GLASS);
                }
                inventory.setItem(22, plugin.getUtil().setMenuTag(createItem(shopItem, shopItem.getItem().getMaxStackSize()), menuOption.getOption().toUpperCase() + ":" + shopItem.getItem().getMaxStackSize()));
                inventory.setItem(24, plugin.getUtil().setMenuTag(createItem(shopItem, amount), "SELL:" + amount));
            }
        }
        inventory.setItem(40, plugin.getModelManager().getMenuOption().getOptions().get("close"));
        for (int i = 0; i < inventory.getSize(); i++) {
            if (inventory.getItem(i) == null || inventory.getItem(i).getType() == Material.AIR) {
                inventory.setItem(i, super.FILLER_GLASS);
            }
        }
    }

    private ItemStack createItem(ShopItem shitem, int ammount) {
        ItemStack item = new ItemStack(shitem.getItem().getType());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(menuOption.getValue().replaceAll("%ammount%", ammount + ""));
        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add(plugin.color("&6&lBuyprice: &a&l" + (shopItem.getBuyprice() * ammount) + "" + (shopItem.getShopCurency() == ShopCurrency.BALANCE ? "$" : shopItem.getShopCurency() == ShopCurrency.EXP ? "EXP" : "TOKENS")));
        lore.add(plugin.color("&6&lSellprice: &a&l" + (shopItem.getSellprice() * ammount) + "" + (shopItem.getShopCurency() == ShopCurrency.BALANCE ? "$" : shopItem.getShopCurency() == ShopCurrency.EXP ? "EXP" : "TOKENS")));
        lore.add("");
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }

    private int getEmptySlots(Player p) {
        PlayerInventory inventory = p.getInventory();
        ItemStack[] cont = inventory.getContents();
        int i = 0;
        for (ItemStack item : cont)
            if (item != null && item.getType() != Material.AIR) {
                i++;
            }
        return 36 - i;
    }

    private int getAmount(Inventory inventory, Material type) {
        ItemStack[] items = inventory.getContents();
        int has = 0;
        for (ItemStack item : items) {
            if ((item != null) && (item.getType() == type) && (item.getAmount() > 0)) {
                has += item.getAmount();
            }
        }
        return has;
    }

    private boolean removeItems(Inventory inventory, Material type, int amount) {
        if (amount <= 0) return false;
        int size = inventory.getSize();
        for (int slot = 0; slot < size; slot++) {
            ItemStack is = inventory.getItem(slot);
            if (is == null) continue;
            if (type == is.getType()) {
                int newAmount = is.getAmount() - amount;
                if (newAmount > 0) {
                    is.setAmount(newAmount);
                    break;
                } else {
                    inventory.clear(slot);
                    amount = -newAmount;
                    if (amount == 0) break;
                }
            }
        }
        return true;
    }

    private boolean hasSpaceForItem(Player p, Material material, int amount) {
        PlayerInventory inventory = p.getInventory();
        ItemStack[] cont = inventory.getContents();
        for (ItemStack item : cont) {
            if (item != null) {
                if (item.getType() == material) {
                    if (item.getAmount() < item.getMaxStackSize() && (item.getAmount() + amount) <= item.getMaxStackSize()) {
                        return true;
                    }
                }
            }
        }
        return getEmptySlots(p) != 0;
    }
}

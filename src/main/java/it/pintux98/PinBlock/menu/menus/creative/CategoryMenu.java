package it.pintux98.PinBlock.menu.menus.creative;

import it.pintux98.PinBlock.menu.PBMenu;
import it.pintux98.PinBlock.menu.PlayerMenuUtility;
import it.pintux98.PinBlock.menu.models.creative.CreativeModel;
import it.pintux98.PinBlock.menu.models.creative.CreativeOption;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

public class CategoryMenu extends PBMenu {

    private final CreativeModel model;

    public CategoryMenu(PlayerMenuUtility playerMenuUtility) {
        super(playerMenuUtility);
        model = plugin.getModelManager().getCreativeModel();
    }

    @Override
    public String getMenuName() {
        return model.getTitle();
    }

    @Override
    public int getRows() {
        return 6;
    }

    @Override
    public void handleMenu(InventoryClickEvent e) {

        for (CreativeOption option : model.getOptions()) {
            String tag = plugin.getUtil().getMenuTag(e.getCurrentItem());
            if (tag != null) {
                if (plugin.getUtil().getMenuTag(e.getCurrentItem()).equalsIgnoreCase(option.getOption())) {
                    new CreativeMenu(playerMenuUtility, option).open();
                }
            }
        }
    }

    @Override
    public void setMenuItems() {
        for (CreativeOption option : model.getOptions()) {
            inventory.setItem(option.getSlot(), plugin.getUtil().setMenuTag(option.getIcon(), option.getOption()));
        }
        for (int i = 0; i < inventory.getSize(); i++) {
            if (inventory.getItem(i) == null || inventory.getItem(i).getType() == Material.AIR) {
                inventory.setItem(i, super.FILLER_GLASS);
            }
        }
    }
}

package it.pintux98.PinBlock.menu.models.menu;

import org.bukkit.inventory.ItemStack;

import java.util.List;

public class MenuItemModel {

    private String menuItemName;
    private ItemStack item;
    private MenuLocation location;
    private int slot;
    private String openMenu;
    private String permission;
    private List<String> commands;

    public MenuItemModel(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    public String getMenuItemName() {
        return menuItemName;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public MenuLocation getLocation() {
        return location;
    }

    public void setLocation(MenuLocation location) {
        this.location = location;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public String getOpenMenu() {
        return openMenu;
    }

    public void setOpenMenu(String openMenu) {
        this.openMenu = openMenu;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public List<String> getCommands() {
        return commands;
    }

    public void setCommands(List<String> commands) {
        this.commands = commands;
    }
}

package it.pintux98.PinBlock.menu.models.menu;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class MenuOption {

    private String title;
    private HashMap<String, ItemStack> options;

    public MenuOption(String title) {
        this.title = title;
        options = new HashMap<>();
    }

    public String getTitle() {
        return title;
    }

    public void addOption(String option, ItemStack stack) {
        options.putIfAbsent(option, stack);
    }

    public HashMap<String, ItemStack> getOptions() {
        return options;
    }
}

package it.pintux98.PinBlock.menu.models.creative;

import it.pintux98.PinBlock.main;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class CreativeOption {

    private String option;
    private String title;
    private ItemStack icon;
    private int slot;
    private List<ItemStack> content;

    public CreativeOption(String option) {
        this.option = option;
    }

    public String getOption() {
        return option;
    }

    public String getTitle() {
        return main.getInst().color(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ItemStack getIcon() {
        return icon;
    }

    public void setIcon(ItemStack icon) {
        this.icon = icon;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public List<ItemStack> getContent() {
        return content;
    }

    public void setContent(List<ItemStack> content) {
        this.content = content;
    }
}

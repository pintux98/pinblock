package it.pintux98.PinBlock.menu.models.shop;

import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ShopItem {

    private ItemStack item;
    private ShopCurrency shopCurency;
    private int buyprice;
    private int sellprice;
    private List<String> buycommands;
    private List<String> sellcommands;

    public ShopItem(ItemStack item) {
        this.item = item;
    }

    public ItemStack getItem() {
        return item;
    }

    public int getBuyprice() {
        return buyprice;
    }

    public void setBuyprice(int buyprice) {
        this.buyprice = buyprice;
    }

    public int getSellprice() {
        return sellprice;
    }

    public void setSellprice(int sellprice) {
        this.sellprice = sellprice;
    }

    public List<String> getBuycommands() {
        return buycommands;
    }

    public void setBuycommands(List<String> buycommands) {
        this.buycommands = buycommands;
    }

    public List<String> getSellcommands() {
        return sellcommands;
    }

    public void setSellcommands(List<String> sellcommands) {
        this.sellcommands = sellcommands;
    }

    public ShopCurrency getShopCurency() {
        return shopCurency;
    }

    public void setShopCurency(ShopCurrency shopCurency) {
        this.shopCurency = shopCurency;
    }

    @Override
    public String toString() {
        return "ShopItem{" +
                "item=" + item +
                ", shopCurency=" + shopCurency +
                ", buyprice=" + buyprice +
                ", sellprice=" + sellprice +
                ", commands=" + buycommands +
                '}';
    }
}

package it.pintux98.PinBlock.menu.models;

import it.pintux98.PinBlock.main;
import it.pintux98.PinBlock.menu.PBMenu;
import it.pintux98.PinBlock.menu.menus.MainMenu;
import it.pintux98.PinBlock.menu.menus.creative.CategoryMenu;
import it.pintux98.PinBlock.menu.menus.shop.ShopMainMenu;
import it.pintux98.PinBlock.menu.menus.shop.ShopOptionMenu;
import it.pintux98.PinBlock.menu.models.creative.CreativeModel;
import it.pintux98.PinBlock.menu.models.creative.CreativeOption;
import it.pintux98.PinBlock.menu.models.menu.MenuItemModel;
import it.pintux98.PinBlock.menu.models.menu.MenuLocation;
import it.pintux98.PinBlock.menu.models.menu.MenuOption;
import it.pintux98.PinBlock.menu.models.shop.*;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MenuModelManager {

    private final main plugin;
    private final FileConfiguration config;
    private CreativeModel creativeModel;
    private ShopModel shopModel;
    private List<MenuItemModel> menuItemModels;
    private MenuOption menuOption;
    private HashMap<String, Class<? extends PBMenu>> menus;

    public MenuModelManager(main plugin) {
        this.plugin = plugin;
        config = plugin.getConfig();
        menus = new HashMap<>();
    }

    public CreativeModel getCreativeModel() {
        return creativeModel;
    }

    public ShopModel getShopModel() {
        return shopModel;
    }

    public List<MenuItemModel> getMenuItemModels() {
        return menuItemModels;
    }

    public MenuOption getMenuOption() {
        return menuOption;
    }

    public HashMap<String, Class<? extends PBMenu>> getMenus() {
        return menus;
    }

    public void loadCreativeModel() {
        long timeMillis = System.currentTimeMillis();
        String optionLoc = "menu.creativemenu.options";
        String menuTitle = config.getString("menu.creativemenu.title");
        creativeModel = new CreativeModel(menuTitle);
        for (String option : config.getConfigurationSection(optionLoc).getKeys(false)) {
            long mills = System.currentTimeMillis();
            String loc = optionLoc + "." + option;
            String optionTitle = config.getString(loc + ".title");
            ItemStack icon = createItemStack(loc);
            int slot = config.getInt(loc + ".slot");
            List<ItemStack> content = new ArrayList<>();
            for (String mat : config.getStringList(loc + ".content")) {
                try {
                    String[] values = mat.split(":");
                    if (values.length == 1) {
                        content.add(new ItemStack(Material.valueOf(mat)));
                    } else {
                        content.add(new ItemStack(Material.valueOf(values[0]), 1, (short) Integer.parseInt(values[1])));
                    }
                } catch (Exception ignored) {
                }
            }
            CreativeOption creativeOption = new CreativeOption(option);
            creativeOption.setTitle(optionTitle);
            creativeOption.setIcon(icon);
            creativeOption.setSlot(slot);
            creativeOption.setContent(content);
            creativeModel.addOption(creativeOption);
            plugin.getLogger().info("Loaded [" + loc + "] model... " + option + " in " + (System.currentTimeMillis() - mills) + " ms");
        }
        menus.put("creativemenu", CategoryMenu.class);
        plugin.getLogger().info("Loaded creative model... in " + (System.currentTimeMillis() - timeMillis) + " ms");
    }

    public void loadMenuOptionModels() {
        long timeMillis = System.currentTimeMillis();
        String optionLoc = "menu.mainmenu.options";
        String menuTitle = config.getString("menu.mainmenu.title");
        menuOption = new MenuOption(menuTitle);
        for (String option : config.getConfigurationSection(optionLoc).getKeys(false)) {
            String loc = optionLoc + "." + option;
            menuOption.addOption(option, createItemStack(loc));
        }
        menus.put("mainmenu", MainMenu.class);
        plugin.getLogger().info("Loaded menu models... in " + (System.currentTimeMillis() - timeMillis) + " ms");
    }

    public void loadMenuModels() {
        long timeMillis = System.currentTimeMillis();
        String optionLoc = "items";
        menuItemModels = new ArrayList<>();
        for (String option : config.getConfigurationSection(optionLoc).getKeys(false)) {
            String loc = optionLoc + "." + option;
            if (config.getBoolean(loc + ".enabled")) {
                MenuItemModel model = new MenuItemModel(option);
                model.setItem(createItemStack(loc));
                model.setCommands(config.getStringList(loc + ".command"));
                try {
                    model.setLocation(MenuLocation.valueOf(config.getString(loc + ".location")));
                } catch (Exception e) {
                    config.set(loc + ".enabled", false);
                    plugin.getLogger().info("Invalid value at [" + loc + ".location] ... skipping this item");
                    return;
                }
                model.setSlot(config.getInt(loc + ".slot"));
                model.setOpenMenu(config.getString(loc + ".openmenu"));
                model.setPermission(config.getString(loc + ".permission"));
                menuItemModels.add(model);
            }
        }
        plugin.getLogger().info("Loaded menu models... in " + (System.currentTimeMillis() - timeMillis) + " ms");
    }

    public void loadShopModel() {
        long timeMillis = System.currentTimeMillis();
        String shopLoc = "menu.shopmenu.shops";
        String menuTitle = "menu.shopmenu.title";
        List<String> itemDescription = new ArrayList<>();
        config.getStringList("menu.shopmenu.itemdescription").forEach(s -> itemDescription.add(plugin.color(s)));
        shopModel = new ShopModel(config.getString(menuTitle));
        for (String key : config.getConfigurationSection("menu.shopmenu.options").getKeys(false)) {
            String value = "menu.shopmenu.options." + key;
            ShopMenuOption menuOption = new ShopMenuOption(key);
            menuOption.setPermission(config.getString(value + ".permission"));
            menuOption.setTitle(config.getString(value + ".title"));
            menuOption.setValue(config.getString(value + ".value"));
            shopModel.addShopMenuOption(menuOption);
        }
        shopModel.setPercent(config.getDouble("menu.shopmenu.percent"));
        for (String option : config.getConfigurationSection(shopLoc).getKeys(false)) {
            long mills = System.currentTimeMillis();
            String loc = shopLoc + "." + option;
            String optionTitle = config.getString(loc + ".title");
            ItemStack icon = createItemStack(loc);
            int slot = config.getInt(loc + ".slot");
            ShopOption shopOption = new ShopOption(option);
            shopOption.setTitle(optionTitle);
            shopOption.setIcon(plugin.getUtil().setMenuTag(icon, option));
            shopOption.setSlot(slot);
            for (String mat : config.getConfigurationSection(loc + ".content").getKeys(false)) {
                String tempLoc = loc + ".content." + mat;
                try {
                    Material.valueOf(mat);
                } catch (Exception ignored) {
                    plugin.getLogger().info("Invalid Material ID for this server version at [" + loc + "." + mat + "] ... skipping this item");
                    continue;
                }
                try {
                    ShopCurrency.valueOf(config.getString(tempLoc + ".currency"));
                } catch (Exception e) {
                    plugin.getLogger().info("Invalid currency at [" + loc + "." + mat + "] ... skipping this item");
                    continue;
                }
                ItemStack itemStack = new ItemStack(Material.valueOf(mat), 1, (short) config.getInt(tempLoc + ".data"));
                ItemMeta itemMeta = itemStack.getItemMeta();
                itemMeta.setLore(itemDescription);
                itemStack.setItemMeta(itemMeta);
                ShopItem shopItem = new ShopItem(itemStack);
                shopItem.setBuyprice(config.getInt(tempLoc + ".buyprice"));
                shopItem.setSellprice(config.getInt(tempLoc + ".sellprice"));
                ShopCurrency shopCurrency = ShopCurrency.valueOf(config.getString(tempLoc + ".currency"));
                shopItem.setShopCurency(shopCurrency);
                shopItem.setBuycommands(config.getStringList(tempLoc + ".buycommands"));
                shopItem.setSellcommands(config.getStringList(tempLoc + ".sellcommands"));
                shopOption.addItem(shopItem);
            }
            menus.put(option, ShopOptionMenu.class);
            shopModel.addShop(shopOption);
            plugin.getLogger().info("Loaded [" + loc + "] shop with " + shopOption.getItems().size() + " items... " + option + " in " + (System.currentTimeMillis() - mills) + " ms");
        }

        menus.put("shopmenu", ShopMainMenu.class);
        plugin.getLogger().info("Loaded shop model... in " + (System.currentTimeMillis() - timeMillis) + " ms");
    }

    private ItemStack createItemStack(String path) {
        Material material;
        try {
            material = Material.valueOf(config.getString(path + ".icon"));
        } catch (Exception e) {
            material = Material.STONE;
            plugin.getLogger().info("Invalid Material ID for this server version at [" + path + ".icon" + "] ... setting it to default STONE");
        }
        ItemStack itemStack = new ItemStack(material);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(plugin.color(config.getString(path + ".name")));
        List<String> lore = new ArrayList<>();
        config.getStringList(path + ".description").forEach(s -> lore.add(plugin.color(s)));
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

}
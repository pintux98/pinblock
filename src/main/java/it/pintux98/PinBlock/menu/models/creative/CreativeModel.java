package it.pintux98.PinBlock.menu.models.creative;

import it.pintux98.PinBlock.main;

import java.util.ArrayList;
import java.util.List;

public class CreativeModel {

    private String title;
    private List<CreativeOption> options;

    public CreativeModel(String title) {
        this.title = title;
        this.options = new ArrayList<>();
    }

    public void addOption(CreativeOption option) {
        if (!options.contains(option)) {
            options.add(option);
        }
    }

    public String getTitle() {
        return main.getInst().color(title);
    }

    public List<CreativeOption> getOptions() {
        return options;
    }
}

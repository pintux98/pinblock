package it.pintux98.PinBlock.menu.models.shop;

import it.pintux98.PinBlock.main;

import java.util.ArrayList;
import java.util.List;

public class ShopModel {

    private String title;
    private List<ShopMenuOption> shopMenuOptions;
    private double percent;

    private List<ShopOption> shops;

    public ShopModel(String title) {
        this.title = title;
        shops = new ArrayList<>();
        shopMenuOptions = new ArrayList<>();
    }

    public String getTitle() {
        return main.getInst().color(title);
    }

    public List<ShopMenuOption> getShopMenuOptions() {
        return shopMenuOptions;
    }

    public void addShopMenuOption(ShopMenuOption shopMenuOption) {
        if (!shopMenuOptions.contains(shopMenuOption)) {
            shopMenuOptions.add(shopMenuOption);
        }
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public List<ShopOption> getShops() {
        return shops;
    }

    public void addShop(ShopOption shop) {
        if (!shops.contains(shop))
            shops.add(shop);
    }
}

package it.pintux98.PinBlock.menu.models.shop;

import it.pintux98.PinBlock.main;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ShopOption {

    private String shop;
    private String title;
    private ItemStack icon;
    private int slot;

    private List<ShopItem> items;

    public ShopOption(String shop) {
        this.shop = shop;
        items = new ArrayList<>();
    }

    public String getShop() {
        return shop;
    }

    public String getTitle() {
        return main.getInst().color(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ItemStack getIcon() {
        return icon;
    }

    public void setIcon(ItemStack icon) {
        this.icon = icon;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public List<ShopItem> getItems() {
        return items;
    }

    public void addItem(ShopItem item) {
        if (!items.contains(item)) {
            items.add(item);
        }
    }
}

package it.pintux98.PinBlock.menu.models.shop;

import it.pintux98.PinBlock.main;

public class ShopMenuOption {

    private String option;
    private String permission;
    private String title;
    private String value;

    public ShopMenuOption(String option) {
        this.option = option;
    }

    public String getOption() {
        return option;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getTitle() {
        return main.getInst().color(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return main.getInst().color(value);
    }

    public void setValue(String value) {
        this.value = value;
    }
}

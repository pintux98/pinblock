package it.pintux98.PinBlock.menu.models.shop;

public enum ShopCurrency {

    BALANCE,
    TOKENS,
    EXP,
    INVALID;

}

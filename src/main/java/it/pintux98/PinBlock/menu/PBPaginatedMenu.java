package it.pintux98.PinBlock.menu;

import it.pintux98.PinBlock.main;
import it.pintux98.PinBlock.menu.models.menu.MenuOption;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public abstract class PBPaginatedMenu extends PBMenu {

    protected int page = 0;

    protected int maxItemsPerPage = 28;

    protected int index = 0;

    public PBPaginatedMenu(PlayerMenuUtility playerMenuUtility) {
        super(playerMenuUtility);
    }

    public void addMenuBorder() {

        MenuOption option = main.getInst().getModelManager().getMenuOption();
        option.getOptions().forEach((s, itemStack) -> {
            switch (s.toUpperCase()) {
                case "LEFT":
                    inventory.setItem(48, itemStack);
                case "RIGHT":
                    inventory.setItem(50, itemStack);
                case "CLOSE":
                    inventory.setItem(49, itemStack);
            }
        });

        for (int i = 0; i < 10; i++) {
            if (inventory.getItem(i) == null) {
                inventory.setItem(i, super.FILLER_GLASS);
            }
        }
        inventory.setItem(17, super.FILLER_GLASS);
        inventory.setItem(18, super.FILLER_GLASS);
        inventory.setItem(26, super.FILLER_GLASS);
        inventory.setItem(27, super.FILLER_GLASS);
        inventory.setItem(35, super.FILLER_GLASS);
        inventory.setItem(36, super.FILLER_GLASS);

        for (int i = 44; i < 54; i++) {
            if (inventory.getItem(i) == null) {
                inventory.setItem(i, super.FILLER_GLASS);
            }
        }

    }

    private ItemStack createItem(String value) {
        ItemStack itemStack;
        try {
            itemStack = new ItemStack(Material.valueOf(plugin.getConfig().getString(value + ".item")));
        } catch (Exception ignored) {
            itemStack = new ItemStack(Material.ARROW);
        }
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(plugin.color(plugin.getConfig().getString(value + ".name")));
        List<String> lore = new ArrayList<>();
        plugin.getConfig().getStringList(value + ".description").forEach(s -> lore.add(plugin.color(s)));
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

}

package it.pintux98.PinBlock.menu;

import it.pintux98.PinBlock.main;
import it.pintux98.PinBlock.menu.models.creative.CreativeOption;
import it.pintux98.PinBlock.menu.models.menu.MenuItemModel;
import it.pintux98.PinBlock.menu.models.shop.ShopModel;
import it.pintux98.PinBlock.menu.models.shop.ShopOption;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class PBMenuListener implements Listener {

    @EventHandler
    public void onMenuClick(InventoryClickEvent e) {
        if (e.getClickedInventory() != null) {
            InventoryHolder holder = e.getClickedInventory().getHolder();
            if (holder instanceof PBMenu) {
                e.setCancelled(true);
                if (e.getCurrentItem() == null) {
                    return;
                }
                PBMenu pbMenu = (PBMenu) holder;
                pbMenu.handleMenu(e);
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        ItemStack item = e.getItem();
        if (item == null || item.getType() == Material.AIR) {
            e.setCancelled(true);
            return;
        }
        for (MenuItemModel model : main.getInst().getModelManager().getMenuItemModels()) {
            if (item.equals(model.getItem())) {
                try {
                    Class<? extends PBMenu> cl = main.getInst().getModelManager().getMenus().get(model.getOpenMenu());
                    Constructor constructor = cl.getConstructor(PlayerMenuUtility.class);
                    Object finale = constructor.newInstance(main.getInst().getPlayerMenuUtility(p));
                    PBMenu menu = cl.cast(finale);
                    menu.open();
                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException ex) {
                    ex.printStackTrace();
                }
                break;
            }
        }
    }

    @EventHandler
    public void onPlayerCommand(PlayerCommandPreprocessEvent e) {
        String command = e.getMessage().replaceAll("/", "");
        for (MenuItemModel model : main.getInst().getModelManager().getMenuItemModels()) {
            for (String cmd : model.getCommands()) {
                String[] args = command.split("\\s");
                if (args.length == 1) {
                    if (cmd.equalsIgnoreCase(command)) {
                        e.setCancelled(true);
                        if (main.getInst().getModelManager().getMenus().containsKey(model.getOpenMenu())) {
                            try {
                                Class<? extends PBMenu> cl = main.getInst().getModelManager().getMenus().get(model.getOpenMenu());
                                Constructor constructor = cl.getConstructor(PlayerMenuUtility.class);
                                Object finale = constructor.newInstance(main.getInst().getPlayerMenuUtility(e.getPlayer()));
                                PBMenu menu = cl.cast(finale);
                                menu.open();
                            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException ex) {
                                ex.printStackTrace();
                            }
                            break;
                        }
                    }
                } else if (args.length == 2) {
                    if (cmd.startsWith(args[0])) {
                        e.setCancelled(true);
                        for (ShopOption option : main.getInst().getModelManager().getShopModel().getShops()) {
                            if (option.getShop().equalsIgnoreCase(args[1])) {
                                try {
                                    Class<? extends PBMenu> cl = main.getInst().getModelManager().getMenus().get(option.getShop());
                                    Constructor constructor = cl.getConstructor(PlayerMenuUtility.class, ShopOption.class, ShopModel.class);
                                    Object finale = constructor.newInstance(main.getInst().getPlayerMenuUtility(e.getPlayer()), option, main.getInst().getModelManager().getShopModel());
                                    PBMenu menu = cl.cast(finale);
                                    menu.open();
                                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException ex) {
                                    ex.printStackTrace();
                                }
                                break;
                            }
                        }
                        for (CreativeOption option : main.getInst().getModelManager().getCreativeModel().getOptions()) {
                            if (option.getOption().equalsIgnoreCase(args[1])) {
                                try {
                                    Class<? extends PBMenu> cl = main.getInst().getModelManager().getMenus().get(option.getOption());
                                    Constructor constructor = cl.getConstructor(PlayerMenuUtility.class, CreativeOption.class);
                                    Object finale = constructor.newInstance(main.getInst().getPlayerMenuUtility(e.getPlayer()), option);
                                    PBMenu menu = cl.cast(finale);
                                    menu.open();
                                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException ex) {
                                    ex.printStackTrace();
                                }
                                break;
                            }
                        }
                    }
                } else break;
            }
        }
    }
}

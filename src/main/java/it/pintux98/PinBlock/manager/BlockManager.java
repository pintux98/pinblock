package it.pintux98.PinBlock.manager;

import com.google.common.collect.ImmutableList;
import it.pintux98.PinBlock.main;
import it.pintux98.PinBlock.util.PBVersion;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BlockManager implements Listener {

    private static final ImmutableList<Object> BLOCK_FACES = ImmutableList.of(BlockFace.UP,
            BlockFace.DOWN, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH,
            BlockFace.WEST);
    private final main plugin;

    public BlockManager(main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (e.isCancelled())
            return;
        Player p = e.getPlayer();

        if (p.hasPermission(plugin.getUtil().getStaffpex()) || p.isOp())
            return;
        if (main.getInst().getConfig().getBoolean("Settings.Disable_Wither")) {
            if (e.getBlock().getType().toString().contains("SKELETON") && this.isAboveSoulSand(e.getBlock())) {
                e.setCancelled(true);
                p.sendMessage(plugin.getMessageUtil().message("CANT_USE_ITEM"));
            } else if (e.getBlock().getType().toString().contains("SOUL") && this.isBelowSkull(e.getBlock())) {
                e.setCancelled(true);
                p.sendMessage(plugin.getMessageUtil().message("CANT_USE_ITEM"));
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (e.isCancelled())
            return;
        Block b = e.getBlock();
        Player p = e.getPlayer();
        if (p.hasPermission(plugin.getUtil().getStaffpex()) || p.isOp())
            return;
        if (b.getLocation().getY() < 1.0) {
            e.setCancelled(true);
            return;
        }
        if (p.hasPermission(plugin.getUtil().getGmcpex())) {
            e.setExpToDrop(0);
            e.setCancelled(true);
            e.getBlock().setType(Material.AIR);
        }
    }

    @EventHandler
    public void onPistonExplode(EntityExplodeEvent event) {
        Set<Block> blockSet = new HashSet<>();
        if (plugin.getVersion() == PBVersion.OLD || plugin.getVersion() == PBVersion.COMBAT) {
            blockSet = event.blockList().stream()
                    .filter(b -> b.getType() == Material.valueOf("PISTON_MOVING_PIECE")
                    ).collect(Collectors.toSet());
        } else if (plugin.getVersion() == PBVersion.NEW) {
            blockSet = event.blockList().stream()
                    .filter(b -> b.getType() == Material.valueOf("LEGACY_PISTON_MOVING_PIECE")
                    ).collect(Collectors.toSet());
        }
        for (final Block b2 : blockSet) {
            BLOCK_FACES.forEach(bF -> this.fixGlitch(b2, bF));
        }
    }

    private void fixGlitch(final Block block, final Object bF) {
        final byte data = block.getState().getData().getData();
        if (plugin.getVersion() == PBVersion.OLD || plugin.getVersion() == PBVersion.COMBAT) {
            if (data > 13 || block.getRelative((BlockFace) bF).getType() != Material.valueOf("PISTON_BASE")) {
                return;
            }
        } else {
            if (data > 13 || block.getRelative((BlockFace) bF).getType() != Material.valueOf("LEGACY_PISTON_BASE")) {
                return;
            }
        }
        block.getRelative((BlockFace) bF).setType(Material.AIR);
    }

    @EventHandler
    public void WitherCreationViaExploit(BlockPistonExtendEvent event) {
        List<Block> blocks;
        try {
            blocks = event.getBlocks();
        } catch (Exception e) {
            blocks = null;
        }
        if (main.getInst().getConfig().getBoolean("Settings.Disable_Wither")) {
            if (blocks != null && blocks.size() > 0) {
                for (Block block : blocks) {
                    final Material type = block.getType();
                    if (type.toString().contains("SOUL") && this.isNearWitherSkull(block)) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    private boolean isAboveSoulSand(final Block block) {
        final Location blockbelowlocation = block.getLocation();
        blockbelowlocation.setY(block.getY() - 1);
        return blockbelowlocation.getBlock().getType().toString().contains("SOUL");
    }

    private boolean isBelowSkull(final Block block) {
        final Location blockabovelocation = block.getLocation();
        blockabovelocation.setY(block.getY() + 1);
        return blockabovelocation.getBlock().getType().toString().contains("SKELETON");
    }

    private boolean isNearWitherSkull(final Block block) {
        final Location blockNearLocation = block.getLocation();
        blockNearLocation.setY(block.getY() + 1);
        blockNearLocation.setX(block.getX() + 1);
        if (blockNearLocation.getBlock().getType().toString().contains("SKELETON")) {
            return true;
        }
        blockNearLocation.setX(block.getX() - 1);
        if (blockNearLocation.getBlock().getType().toString().contains("SKELETON")) {
            return true;
        }
        blockNearLocation.setX(block.getX());
        blockNearLocation.setZ(block.getZ() + 1);
        if (blockNearLocation.getBlock().getType().toString().contains("SKELETON")) {
            return true;
        }
        blockNearLocation.setZ(block.getZ() - 1);
        return blockNearLocation.getBlock().getType().toString().contains("SKELETON");
    }
}
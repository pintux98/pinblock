package it.pintux98.PinBlock.manager;

import it.pintux98.PinBlock.main;
import it.pintux98.PinBlock.util.PBPlayer;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;

import java.util.Optional;

public class PlayerManager implements Listener {

    private final main plugin;

    public PlayerManager(main plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCommand(PlayerCommandPreprocessEvent e) {
        if (e.isCancelled())
            return;
        Player p = e.getPlayer();
        if (p.hasPermission(plugin.getUtil().getStaffpex()) || p.isOp())
            return;
        for (final String block : plugin.getConfig().getStringList("AntiFreecam")) {
            try {
                if (p.getLocation().getBlock().getType() == Material.valueOf(block)) {
                    for (final String command : plugin.getConfig().getStringList("disabled_on_antifreecam_blocks")) {
                        if (e.getMessage().equalsIgnoreCase(command)) {
                            p.closeInventory();
                            p.sendMessage(plugin.getMessageUtil().message("NO_COMMAND_ON_ANTIFREECAM_BLOCK")
                                    .replaceAll("%block%", block));
                            e.setCancelled(true);
                            return;
                        }
                    }
                }
            } catch (Exception ignored) {
            }
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        plugin.getPbPlayers().removeIf(pbPlayer -> pbPlayer.getPlayer().equals(e.getPlayer()));
    }

    @EventHandler
    public void onKick(PlayerKickEvent e) {
        plugin.getPbPlayers().removeIf(pbPlayer -> pbPlayer.getPlayer().equals(e.getPlayer()));
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Optional<PBPlayer> pbPlayer = plugin.getPbPlayers().parallelStream().findFirst().filter(pbPlayer1 -> pbPlayer1.getPlayer().equals(e.getEntity()));
        pbPlayer.ifPresent(player -> plugin.getPbPlayers().remove(player));
    }

    @EventHandler
    public void onPick(PlayerPickupItemEvent e) {
        if (e.isCancelled())
            return;
        Player p = e.getPlayer();
        ItemStack item = e.getItem().getItemStack();
        if (p.hasPermission(plugin.getUtil().getStaffpex()) || p.isOp())
            return;
        if (item != null && item.getType() != Material.AIR) {
            if (plugin.getUtil().hasTag(item)) {
                if (!plugin.getUtil().match(item, p)) {
                    e.setCancelled(true);
                }
            } else {
                if (p.hasPermission(plugin.getUtil().getGmcpex())) {
                    e.getItem().setItemStack(plugin.getUtil().setTag(item, p));
                }
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.isCancelled())
            return;
        Player p = e.getPlayer();
        ItemStack item = e.getItem();
        if (e.getClickedBlock() != null && e.getClickedBlock().getType() != Material.AIR) {
            Optional<PBPlayer> pbPlayer = plugin.getPbPlayers().parallelStream().findFirst().filter(pbPlayer1 -> pbPlayer1.getPlayer().equals(p));
            pbPlayer.ifPresent(player -> {
                if (plugin.getWorldGuardPlugin() != null) {
                    if (plugin.getWorldGuardPlugin().canBuild(p, e.getClickedBlock())) {
                        if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
                            if (e.getClickedBlock() != null) {
                                e.getClickedBlock().setType(Material.AIR);
                            }
                        }
                    }
                }
            });
        }
        if (item != null && item.getType() != Material.AIR) {
            if (plugin.getUtil().hasTag(item) && (!p.hasPermission(plugin.getUtil().getStaffpex()) || !p.isOp())) {
                if (!plugin.getUtil().match(item, p)) {
                    e.setCancelled(true);
                    e.getItem().setType(Material.AIR);
                    p.updateInventory();
                }
            }
        }
    }
    /*private boolean isItem(final ItemStack itemStack, String endsWith) {
        if (itemStack == null)
            return false;
        final String typeNameString = itemStack.getType().name();
        return typeNameString.endsWith(endsWith);
    }*/

    @EventHandler
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        if (e.isCancelled())
            return;
        Player p = e.getPlayer();
        ItemStack item = p.getItemInHand();
        if (!p.hasPermission(plugin.getUtil().getStaffpex()) || !p.isOp()) {
            if ((e.getRightClicked().getType() == EntityType.ITEM_FRAME
                    || e.getRightClicked().getType() == EntityType.ARMOR_STAND) && item != null
                    && item.getType() != Material.AIR) {
                if (plugin.getUtil().hasTag(item)) {
                    e.setCancelled(true);
                } else {
                    if (p.hasPermission(plugin.getUtil().getGmcpex())) {
                        e.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onItemUse(PlayerItemHeldEvent e) {
        Player p = e.getPlayer();
        ItemStack item = p.getItemInHand();
        if (p.hasPermission(plugin.getUtil().getStaffpex()) || p.isOp())
            return;
        if (item != null && item.getType() != Material.AIR)
            if (plugin.getUtil().hasTag(item)) {
                if (!plugin.getUtil().match(item, p)) {
                    p.setItemInHand(null);
                    p.updateInventory();
                }
            } else {
                if (p.hasPermission(plugin.getUtil().getGmcpex())) {
                    p.setItemInHand(plugin.getUtil().setTag(item, p));
                    p.updateInventory();
                }
            }
    }

    @EventHandler
    public void onConsume(PlayerItemConsumeEvent e) {
        if (e.isCancelled())
            return;
        Player p = e.getPlayer();
        ItemStack item = e.getItem();
        if (item != null) {
            if (item.getType() == Material.POTION) {
                PotionMeta meta = (PotionMeta) item.getItemMeta();
                if (meta.hasCustomEffects()) {
                    if (plugin.getUtil().hasTag(item)) {
                        e.setCancelled(true);
                        p.sendMessage(plugin.getMessageUtil().message("CANT_USE_ITEM"));
                        p.getInventory().remove(item);
                    }
                }
            }/* else if (item.getType().toString().equalsIgnoreCase(plugin.getConfig().getString("menu.item"))) {
                if (item.hasItemMeta() && item.getItemMeta().getDisplayName().equals(plugin.color(plugin.getConfig().getString("menu.name")))) {
                    e.setCancelled(true);
                    new CategoryMenu(plugin.getPlayerMenuUtility(p)).open();
                }
            }*/
        }
    }

    /*
    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        if (e.isCancelled())
            return;
        Player p = e.getPlayer();
        ItemStack item = e.getItemDrop().getItemStack();
        if (item != null) {
            if (item.getType().toString().equalsIgnoreCase(plugin.getConfig().getString("menu.item"))) {
                if (item.hasItemMeta() && item.getItemMeta().getDisplayName().equals(plugin.color(plugin.getConfig().getString("menu.name")))) {
                    e.setCancelled(true);
                    new CategoryMenu(plugin.getPlayerMenuUtility(p)).open();
                }
            }
        }
    }*/
}

package it.pintux98.PinBlock.manager;

import it.pintux98.PinBlock.main;
import it.pintux98.PinBlock.util.PBVersion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

public class InventoryManager implements Listener {

    private final main plugin;

    public InventoryManager(main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void inventoryClick(InventoryClickEvent e) {
        if (e.isCancelled())
            return;
        Player p = (Player) e.getWhoClicked();
        Inventory inv = e.getInventory();
        InventoryView view = e.getView();
        ItemStack cursor = p.getItemOnCursor();
        ItemStack current = e.getCurrentItem();
        if (p.hasPermission(plugin.getUtil().getStaffpex()) || p.isOp())
            return;
        if (inv != null) {
            if (plugin.getVersion() == PBVersion.NEW) {
                if (e.getView() != null)
                    if (e.getView().getTitle().contains("&") || e.getView().getTitle().contains("§") || e.getView().getTitle().contains("Inventory") || e.getView().getTitle().contains("Equipped")) {
                        return;
                    }
            } else {
                if (e.getInventory().getTitle().contains("&") || e.getInventory().getTitle().contains("§") || e.getInventory().getName().contains("Inventory") || e.getInventory().getName().contains("Equipped")) {
                    return;
                }
            }
            if (view.getType() == InventoryType.ANVIL) {
                AnvilInventory ai = (AnvilInventory) e.getView().getTopInventory();
                if (ai != null) {
                    ItemStack item = e.getCurrentItem();
                    if (item != null && item.getType() != Material.AIR) {
                        for (ItemStack i : ai.getContents()) {
                            if (plugin.getUtil().hasTag(i)) {
                                ItemStack item2 = ai.getItem(2);
                                if (item2 != null && item2.getType() != Material.AIR)
                                    if (!plugin.getUtil().hasTag(item2)) {
                                        item2 = plugin.getUtil().setTag(item2, p);
                                        ai.setItem(2, item2);
                                        p.updateInventory();
                                        return;
                                    }
                            }
                        }
                    }
                }
            }
            if (p.hasPermission(plugin.getUtil().getGmcpex())) {
                if (cursor != null && cursor.getType() != Material.AIR) {
                    if (!plugin.getUtil().hasTag(cursor)) {
                        cursor = plugin.getUtil().setTag(cursor, p);
                        p.setItemOnCursor(cursor);
                        p.updateInventory();
                    }
                } else if (current != null && current.getType() != Material.AIR) {
                    if (!plugin.getUtil().hasTag(current)) {
                        current = plugin.getUtil().setTag(current, p);
                        e.setCurrentItem(current);
                        p.updateInventory();
                    }
                }
            }
        }
    }

    @EventHandler
    public void onFurnace(FurnaceSmeltEvent e) {
        if (e.isCancelled())
            return;
        ItemStack smelt = e.getSource();
        ItemStack result = e.getResult();
        Player p = plugin.getUtil().getTag(smelt);
        if (p != null) {
            if (p.hasPermission(plugin.getUtil().getStaffpex()) || p.isOp())
                return;
            if (plugin.getUtil().hasTag(smelt)) {
                result = plugin.getUtil().setTag(result, p);
                e.setResult(result);
            }
        }
    }

    @EventHandler
    public void onDispense(BlockDispenseEvent e) {
        ItemStack item = e.getItem();
        if (item.getType().toString().contains("SHULKER_BOX")) {
            if (plugin.getUtil().hasTag(item)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPreCraft(PrepareItemCraftEvent e) {
        for (final HumanEntity he : e.getViewers()) {
            if (he instanceof Player) {
                Player p = (Player) he;
                if (p.hasPermission(plugin.getUtil().getStaffpex()) || p.isOp())
                    return;
                for (ItemStack i : e.getInventory().getMatrix()) {
                    if (i != null && i.getType() != Material.AIR) {
                        if (plugin.getUtil().hasTag(i)) {
                            e.getInventory().setResult(new ItemStack(Material.AIR));
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onOpen(InventoryOpenEvent e) {
        if (e.isCancelled()) {
            return;
        }

        Player p = (Player) e.getPlayer();
        if (p.hasPermission(plugin.getUtil().getStaffpex()) || p.isOp())
            return;
        if (plugin.getVersion() == PBVersion.NEW) {
            if (e.getView() != null)
                if (e.getView().getTitle().contains("&") || e.getView().getTitle().contains("§") || e.getView().getTitle().contains("Inventory") || e.getView().getTitle().contains("Equipped")) {
                    return;
                }
        } else {
            if (e.getInventory().getTitle().contains("&") || e.getInventory().getTitle().contains("§") || e.getInventory().getName().contains("Inventory") || e.getInventory().getName().contains("Equipped")) {
                return;
            }
        }
        for (ItemStack i : e.getInventory().getContents()) {
            if (plugin.getConfig().getBoolean("Settings.IllegalItem.Enabled")) {
                for (final String s : plugin.getConfig().getStringList("Settings.IllegalItem.Check_For_Items")) {
                    try {
                        if (i.getType() == Material.valueOf(s)) {
                            if (i.getAmount() >= plugin.getConfig().getInt("Settings.IllegalItem.Ammount_Check")) {
                                e.getInventory().remove(i);
                                System.out.println(ChatColor.stripColor(plugin.getMessageUtil().message("ILLEGAL_ITEM_STACK"))
                                        .replaceAll("%player%", p.getName()).replaceAll("%item%", "[" + i.getAmount() + "] " + i.getType()));
                                ;
                                for (Player player : Bukkit.getOnlinePlayers()) {
                                    if (player.hasPermission("pinblock.alert") || player.isOp() || player.hasPermission("pinblock.staff")) {
                                        player.sendMessage(plugin.getMessageUtil().message("ILLEGAL_ITEM_STACK")
                                                .replaceAll("%player%", p.getName()).replaceAll("%item%", "[" + i.getAmount() + "] " + i.getType()))
                                        ;
                                    }
                                }
                            }
                        }
                    } catch (Exception ignored) {
                    }
                }
            }
        }
    }
}
